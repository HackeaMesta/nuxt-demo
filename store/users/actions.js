export default {
  getUsers({commit}, page) {
    this.$axios.get(`https://reqres.in/api/users?page=${page}`)
      .then(response => {
        commit('setUsers', response.data.data)
      });
  }
}
