export default {
  getTasksByUser({commit}, userId) {
    return this.$axios.get(`https://jsonplaceholder.typicode.com/todos?userId=${userId}`)
      .then(response => {
        commit('setTasks', response.data);
      });
  },
  toggleTask({commit}, task) {
    return this.$axios({
      url: `https://jsonplaceholder.typicode.com/todos/${task.id}`,
      method: "put",
      data: {
        userId: task.userId,
        id: task.id,
        title: task.title,
        completed: !task.completed
      }
    }).then(response => {
      commit('updateTaskStatus', response.data);
    })
  }
}
