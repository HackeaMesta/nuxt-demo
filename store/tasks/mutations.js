export default {
  setTasks(state, tasks) {
    state.tasks = tasks;
  },
  updateTaskStatus(state, task) {
    const index = state.tasks.findIndex(task => task.id === task.id);
    state.tasks[index].completed = task.completed;
  }
}
