export default {
  setUser(state, user) {
    state.user = user;
  },

  setToken(state, token) {
    state.token = token
  },

  removeUser(state) {
    state.user = {};
    state.token = null;
  }
}
