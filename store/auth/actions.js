export default {
  login({commit}, {username, password}) {
    return new Promise((resolve, reject) => {
      this.$axios({
        url: 'https://reqres.in/api/login',
        method: 'POST',
        data: {
          email: username,
          password
        }
      }).then(response => {
        console.log(response.data);
        commit('setUser', username);
        commit('setToken', response.data.token);
        resolve();
      }).catch(_ => reject());
    });
  }
}
